/**
 *  maxxidom Popup v1.5.7
 *  Autor: Alexander Bechthold
 *  Date: 02.10.2017
 *  UpDate: 13.10.2017
 */

function MaxxidomPopup(options) {

    var popup      = this;
    var open       = 'maxxidom-open';
    var modal      = 'maxxidom-modal';
    var overlay    = 'maxxidom-overlay';
    var createElem = 'div';
    var parentElem = document.querySelector('body');
    var modalExternClass = '';

    this.classActive  = options.classActive  || open;
    this.createPopup  = options.createPopup  || false;
    this.activeScroll = options.activeScroll || false;

    this.modal   = document.querySelector(options.modal)   || '.' + modal;
    this.overlay = document.querySelector(options.overlay) || '.' + overlay;


    if(options.createPopup){

        if(!document.querySelector('.' + overlay) || !document.querySelector(options.overlay)){
            this.overlay = document.createElement(createElem);
            this.overlay.classList.add(options.overlay);
            parentElem.insertBefore(this.overlay, parentElem.firstChild);
        }

        if(!document.querySelector('.' + modal) || !document.querySelector(options.modal)){
            this.modal = document.createElement(createElem);
            this.modal.classList.add(options.modal);
            parentElem.insertBefore(this.modal, parentElem.firstChild);
        }

    }


    if(options.modal !== '.' + modal) this.modal.classList.add(modal);
    if(options.overlay !== '.' + overlay) this.overlay.classList.add(overlay);


    this.open = function(content) {

        popup.modal.innerHTML = content;
        popup.modal.classList.add(popup.classActive);
        popup.overlay.classList.add(popup.classActive);

        if(!popup.activeScroll) {
            var x = window.scrollX;
            var y = window.scrollY;

            window.onscroll = function() { window.scrollTo(x, y) };
        }

        return this;

    };


    this.externClass = function(content){

        modalExternClass = content || 'class-content';
        popup.modal.classList.add(modalExternClass);

        return this;

    };


    this.close = function() {

        popup.modal.classList.remove(popup.classActive);
        popup.overlay.classList.remove(popup.classActive);

        if(modalExternClass){
            popup.modal.classList.remove(modalExternClass);
            modalExternClass = '';
        }

        if(!popup.activeScroll){
            window.onscroll = function() { return true; };
        }

        return this;

    };


    this.overlay.onclick = popup.close;

}