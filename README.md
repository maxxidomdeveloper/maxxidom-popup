## Установка пакета maxxidom-popup через установщик пакетов bower.
    bower i maxxidom-popup
    
## Установка пакета maxxidom-popup через git.
    git clone https://Maxxidom@bitbucket.org/maxxidomdeveloper/maxxidom-popup.git
    
##HTML File
### Установить два дива сразу после тега BODY.
    <div class="maxxidom-modal"></div>
    <div class="maxxidom-overlay"></div>

##JS File
    var MaxxPopup = new MaxxidomPopup({
        modal: '.maxxidom-modal',
        overlay: '.maxxidom-overlay'
    });
    
### Метод open.
    // Пример.
    $('a').on('click', function(e){
        MaxxPopup.open('content');
    });
    
    // Пример.
    $('a').on('click', function(e){
        MaxxPopup.open('<i class="fa fa-window-close "></i>');
    });
    
### Метод close.
    $('.maxxidom-modal').on('click', '.fa-window-close', function(){
        MaxxPopup.close();
    });
    
    
#### Version 1.5.5
>Update: 10.10.2017
#### Version 1.5.6
>Update: 11.10.2017
#### Version 1.5.7
>Update: 13.10.2017
>
>createPopup: (boolean) true = создает в body два дива с классами  .maxxidom-modal и .maxxidom-overlay
>
>externClass: Добавление декоративного класса. 

##JS File
    var MaxxPopup = new MaxxidomPopup({
        modal: '.maxxidom-modal',
        overlay: '.maxxidom-overlay',
        classActive: 'maxxidom-open',
        activeScroll: false,
        createPopup: false
    });
    
    $('a').on('click', function(e){
        MaxxPopup.open('content').externClass('class-content');
    });
